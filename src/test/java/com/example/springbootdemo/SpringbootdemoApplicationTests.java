package com.example.springbootdemo;

import com.example.springbootdemo.dao.CardMiniMapper;
import com.example.springbootdemo.dao.DeptMapper;
import com.example.springbootdemo.enity.CardMini;
import com.example.springbootdemo.enity.Dept;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootdemoApplicationTests {
@Autowired
    DeptMapper deptMapper;
    CardMiniMapper cardMiniMapper;
    @Test
    public void contextLoads() {
    }

    @Test
    public void test(){
        List<Dept> list = deptMapper.getAll();
        System.out.println(list.get(0));
    }

    @Test
    public void test1(){
        List<CardMini> list = cardMiniMapper.getAll();

        System.out.println(list.get(0));
    }

}
