package com.example.springbootdemo.controller;

import com.example.springbootdemo.enity.Dept;
import com.example.springbootdemo.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("Dept")
public class DeptController {
    @Autowired
    DeptService deptService;

    @RequestMapping(value = "getAllDepts" , method = RequestMethod.GET)
    @ResponseBody
    public List<Dept> getAllDepts(){
        List<Dept> list =deptService.getAll();
        return  list;
    }
}
