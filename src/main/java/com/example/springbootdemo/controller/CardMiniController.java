package com.example.springbootdemo.controller;


import com.example.springbootdemo.dao.CardMiniMapper;
import com.example.springbootdemo.enity.CardMini;
import com.example.springbootdemo.enity.Dept;
import com.example.springbootdemo.service.CardMiniService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("CardMini")
public class CardMiniController {
    @Autowired
    CardMiniService cardMiniService;

    @RequestMapping(value = "getAllCardMini" , method = RequestMethod.GET)
    @ResponseBody
    public List<CardMini> getAllCardMini() {
        List<CardMini> list = cardMiniService.getAll();
        return list;
    }
}
