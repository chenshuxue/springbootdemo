package com.example.springbootdemo.controller;


import com.example.springbootdemo.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("Unit")
public class UnitController {
    @Autowired
    UnitService unitService;

    @RequestMapping(value = "getAllUnits" ,method = RequestMethod.GET)
    @ResponseBody
    public List getAllUnits(){
        return  unitService.getAll();
    }
}
