package com.example.springbootdemo.dao;

import com.example.springbootdemo.enity.Unit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UnitMapper {

    List<Unit> getAll();


}
