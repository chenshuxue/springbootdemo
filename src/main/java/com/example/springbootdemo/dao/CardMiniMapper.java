package com.example.springbootdemo.dao;

import com.example.springbootdemo.enity.CardMini;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CardMiniMapper {
    List<CardMini> getAll();
}
