package com.example.springbootdemo.enity;

public class Dept {
    private String UNIT_CODE;
    private String DEPT_CODE;
    private String DEPT_NAME;
    private String DEPT_MEMO;
    private Unit unit;



    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUNIT_CODE() {
        return UNIT_CODE;
    }

    public void setUNIT_CODE(String UNIT_CODE) {
        this.UNIT_CODE = UNIT_CODE;
    }

    public String getDEPT_CODE() {
        return DEPT_CODE;
    }

    public void setDEPT_CODE(String DEPT_CODE) {
        this.DEPT_CODE = DEPT_CODE;
    }

    public String getDEPT_NAME() {
        return DEPT_NAME;
    }

    public void setDEPT_NAME(String DEPT_NAME) {
        this.DEPT_NAME = DEPT_NAME;
    }

    public String getDEPT_MEMO() {
        return DEPT_MEMO;
    }

    public void setDEPT_MEMO(String DEPT_MEMO) {
        this.DEPT_MEMO = DEPT_MEMO;
    }
}
