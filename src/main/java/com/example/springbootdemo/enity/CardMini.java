package com.example.springbootdemo.enity;

import java.util.List;

public class CardMini {
    private String UNIT_CODE;
    private Integer CARD_INDX;
    private Integer MINI_INDX;
    private String DEPT_CODE;
    private String MINI_NAME;
    private String MINI_MEMO;
    private List<Unit> Units;
    private List<Dept> Depts;
    private List<Card> Cards;

    public List<Dept> getDepts() {
        return Depts;
    }

    public void setDepts(List<Dept> depts) {
        Depts = depts;
    }

    public List<Card> getCards() {
        return Cards;
    }

    public void setCards(List<Card> cards) {
        Cards = cards;
    }

    public List<Unit> getUnits() {
        return Units;
    }

    public void setUnits(List<Unit> units) {
        Units = units;
    }

    public String getUNIT_CODE() {
        return UNIT_CODE;
    }

    public void setUNIT_CODE(String UNIT_CODE) {
        this.UNIT_CODE = UNIT_CODE;
    }

    public Integer getCARD_INDX() {
        return CARD_INDX;
    }

    public void setCARD_INDX(Integer CARD_INDX) {
        this.CARD_INDX = CARD_INDX;
    }

    public Integer getMINI_INDX() {
        return MINI_INDX;
    }

    public void setMINI_INDX(Integer MINI_INDX) {
        this.MINI_INDX = MINI_INDX;
    }

    public String getDEPT_CODE() {
        return DEPT_CODE;
    }

    public void setDEPT_CODE(String DEPT_CODE) {
        this.DEPT_CODE = DEPT_CODE;
    }

    public String getMINI_NAME() {
        return MINI_NAME;
    }

    public void setMINI_NAME(String MINI_NAME) {
        this.MINI_NAME = MINI_NAME;
    }

    public String getMINI_MEMO() {
        return MINI_MEMO;
    }

    public void setMINI_MEMO(String MINI_MEMO) {
        this.MINI_MEMO = MINI_MEMO;
    }
}
