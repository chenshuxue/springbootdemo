package com.example.springbootdemo.enity;

public class Card {
    private String UNIT_CODE;
    private Integer CARD_INDX;
    private String  CARD_NAME;
    private String  CARD_MEMO;

    public Card(String UNIT_CODE, Integer CARD_INDX, String CARD_NAME, String CARD_MEMO) {
        this.UNIT_CODE = UNIT_CODE;
        this.CARD_INDX = CARD_INDX;
        this.CARD_NAME = CARD_NAME;
        this.CARD_MEMO = CARD_MEMO;
    }

    public String getUNIT_CODE() {
        return UNIT_CODE;
    }

    public void setUNIT_CODE(String UNIT_CODE) {
        this.UNIT_CODE = UNIT_CODE;
    }

    public Integer getCARD_INDX() {
        return CARD_INDX;
    }

    public void setCARD_INDX(Integer CARD_INDX) {
        this.CARD_INDX = CARD_INDX;
    }

    public String getCARD_NAME() {
        return CARD_NAME;
    }

    public void setCARD_NAME(String CARD_NAME) {
        this.CARD_NAME = CARD_NAME;
    }

    public String getCARD_MEMO() {
        return CARD_MEMO;
    }

    public void setCARD_MEMO(String CARD_MEMO) {
        this.CARD_MEMO = CARD_MEMO;
    }
}
