package com.example.springbootdemo.enity;

import java.util.List;

public class Unit {
    private String UNIT_CODE;
    private String UNIT_NAME;
    private String UNIT_MEMO;


    public Unit(String UNIT_CODE, String UNIT_NAME, String UNIT_MEMO) {
        this.UNIT_CODE = UNIT_CODE;
        this.UNIT_NAME = UNIT_NAME;
        this.UNIT_MEMO = UNIT_MEMO;
    }

    public String getUNIT_CODE() {
        return UNIT_CODE;
    }

    public void setUNIT_CODE(String UNIT_CODE) {
        this.UNIT_CODE = UNIT_CODE;
    }

    public String getUNIT_NAME() {
        return UNIT_NAME;
    }

    public void setUNIT_NAME(String UNIT_NAME) {
        this.UNIT_NAME = UNIT_NAME;
    }

    public String getUNIT_MEMO() {
        return UNIT_MEMO;
    }

    public void setUNIT_MEMO(String UNIT_MEMO) {
        this.UNIT_MEMO = UNIT_MEMO;
    }
}
