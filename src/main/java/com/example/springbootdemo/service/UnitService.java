package com.example.springbootdemo.service;

import com.example.springbootdemo.enity.Unit;

import java.util.List;

public interface UnitService {
    List<Unit> getAll();
}
