package com.example.springbootdemo.service;

import com.example.springbootdemo.enity.Dept;

import java.util.List;

public interface DeptService {
    List<Dept>  getAll();
}
