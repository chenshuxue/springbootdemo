package com.example.springbootdemo.service.impl;

import com.example.springbootdemo.dao.CardMiniMapper;
import com.example.springbootdemo.enity.CardMini;
import com.example.springbootdemo.service.CardMiniService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CardMiniService")
public class CardMiniServiceImpl implements CardMiniService {
    @Autowired
    CardMiniMapper cardMiniMapper;

    @Override
    public List<CardMini> getAll() {
        List<CardMini> list = cardMiniMapper.getAll();
        return list;
    }
}
