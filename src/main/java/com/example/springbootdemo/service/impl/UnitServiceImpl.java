package com.example.springbootdemo.service.impl;

import com.example.springbootdemo.dao.UnitMapper;
import com.example.springbootdemo.enity.Unit;
import com.example.springbootdemo.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitServiceImpl implements UnitService {
    @Autowired
    UnitMapper unitMapper;

    @Override
    public List<Unit> getAll() {
        List<Unit> list = null;
        list = unitMapper.getAll();
        return list;
    }
}
