package com.example.springbootdemo.service.impl;

import com.example.springbootdemo.dao.DeptMapper;
import com.example.springbootdemo.enity.Dept;
import com.example.springbootdemo.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("DeptService")
public class DeptServiceImpl implements DeptService {
    @Autowired
    DeptMapper deptMapper;

    @Override
    public List<Dept> getAll() {
        List<Dept> list = null;
        list = deptMapper.getAll();
        return list;
    }
}
