package com.example.springbootdemo.service;

import com.example.springbootdemo.enity.CardMini;

import java.util.List;

public interface CardMiniService {
    List<CardMini> getAll();
}
